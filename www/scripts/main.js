/*global cordova:false */
/*global SyncService:false */

var LSCP = window.LSCP || {};
LSCP.Locations = LSCP.Locations || {};

/* ---------------------------------------------------------
* CONFIGURATION
* Please adapt the following settings to your project.
* */

LSCP.Auth = {

  // The credentials below are used to authenticate requests to the backend.
  username: '', // define a username here
  password: '', // define a password here

  // The password below is used to protect access to the app settings.
  dashboard_password: '' // you can customize it

};

// Your backend base URL (for instance: http://mybackend.com)
// LSCP.Locations.Backend = 'http://idevxxi.acristia.org';
//LSCP.Locations.Backend = 'http://192.168.0.170:3001';
// LSCP.Locations.Backend = 'http://192.168.0.187:3001';
LSCP.Locations.Backend = 'http://ciipme-voc.wnpower.host';


/* End of CONFIGURATION, please do not change anything below this line.
 * --------------------------------------------------------- */

/* AUTH */
LSCP.Auth.headers = {
  'Authorization': "Basic " + btoa(LSCP.Auth.username + ":" + LSCP.Auth.password)
};
$.ajaxSetup({
  headers: LSCP.Auth.headers
});

/* COLLECTIONS */
LSCP.Collection = LSCP.Collection || {};

/* MODELS */
LSCP.Model = LSCP.Model || {};

/* VIEWS */
LSCP.View = LSCP.View || {};

/* CONFIG */
LSCP.Config = LSCP.Config || {};

/* LOCATIONS */
LSCP.Locations.Templates = '/templates/';
LSCP.Locations.JSON = '/data/';
LSCP.Locations.Images = 'images/';
LSCP.Locations.Sounds = 'audio/';

/* EVENTS */
LSCP.Events = {
  APP_LOADING : "APP_LOADING"
};


// Wait for both jQuery and Phonegap ready events
// http://stackoverflow.com/a/10046671/1789900

var jqReady = $.Deferred();
var pgReady = $.Deferred();

var isInBrowser = document.URL.match(/^https?:/);

if (isInBrowser) {
  pgReady.resolve();
} else {
  document.addEventListener('deviceready', pgReady.resolve, false);
}

$(document).bind('ready', jqReady.resolve);

$.when(jqReady, pgReady).then(function () {

  // Disable AJAX cache
  //$.ajaxSetup({ cache: false });

  LSCP.App = new LSCP.View.Base();
  persistence.store.websql.config(persistence, 'idevxxi', 'Local database for iDevXXI', 25 * 1024 * 1024);
  persistence.schemaSync();

  /* SYNC */
  LSCP.Sync = new SyncService();

  /* LOCATIONS */
  window.resolveLocalFileSystemURL(cordova.file.dataDirectory, function(fileSystem){
    var base = fileSystem.nativeURL;

    if ( base == "//POLYFILL_NATIVE_URL/" ) {
      base = LSCP.Locations.Backend + "/";  //usar el mismo backend si estamos en web como para testear
    }


    LSCP.Locations.Root = base;
    LSCP.Locations.GameObjectImages = base + 'game_objects/images/';
    LSCP.Locations.GameObjectSoundSprites = base + 'game_objects/sound_sprites/';

    LSCP.Sync.loadLocalConfig(); //esto adentro tiene LSCP.App.showFullID();
  });

  /* SYNC */
  Backbone.sync = function (method, models, options) {
    var dao = new DAO();
    options = options || {};
    switch (method) {
      case 'find':
        console.log('sync find');
        if ('id' in options) {
          console.log('sync find one');
          return dao.findById(models, options.id);
        } else {
          console.log('sync find all');
          return dao.findAll(models);
        }
        return;

      case 'count':
        console.log('sync count');
        return dao.count(models);

      case 'create':
        console.log('sync create');
        return dao.create(models);

      case 'update':
        console.log('sync update');
        return dao.update(models, options);

      case 'delete':
        console.log('sync delete');
        return dao.delete(models);
    }
  };
});

LSCP.SessionController = LSCP.SessionController || {

    current_session: null,
    current_game: null,
    current_game_view: null,

    init: function(){
        console.log('LSCP.SessionController initialized!');
    },

    startSession: function(){

//        this.current_session = new LSCP.Model.Session({
//            duration: 15 * 60
//        });
//
//        console.log(''startSession', this.current_session.attributes);
//
//        this.current_game = this.current_session.games.shift();
//
//        switch (this.current_game.get('type')) {
//
//            case 'WordComprehensionGame':
//                this.current_game_view = new LSCP.View.WordComprehensionGame({
//                    model: this.current_game
//                });
//
//        }
//
//        $(this.current_game_view);

    }
};

LSCP.Model.Config = Backbone.Model.extend({

  defaults: {
    name: "",
    content: null
  },

  initialize: function(){
  },

});

LSCP.Model.Device = Backbone.Model.extend({
	
	defaults: {
        name: "Device",
        uuid: "unavailable",
        os_version: "unavailable",
        device_name: "Device name"
	},
	
	initialize: function(){
        this.name = 'My Device';
        this.uuid = '0123456789';
        this.os_version = 'v7.0';
        this.device_name = 'iPad de Etamin Studio';

        console.log('LSCP.Model.Device initialized!', JSON.stringify(this));

        this.on('change', function(){
            console.log('LSCP.Model.Device changed');
        });

        // TODO: send it to server if online
        // TODO: retrieve assigned config if online and available
	}

});
LSCP.Model.Game = Backbone.AssociatedModel.extend({
	
	initialize: function(){
    console.log('LSCP.Model.Game initialized!');
	}

});

LSCP.Model.GameObject = Backbone.Model.extend({

  defaults: {
    name: null,
    image: null,
    sound_sprite: null,
    downloaded: false
  },
	
	initialize: function(){
	}

});

LSCP.Model.GameSessionPersist = persistence.define('GameSession', {
  full_id: "TEXT",
  started_at: 'DATE',
  ended_at: 'DATE',
  time_limit: 'INT',
  levels: 'JSON',
  trials: 'JSON',
  events: 'JSON',
  config: 'INT',
  subject: 'TEXT',
  progress: 'INT',
  synced: 'BOOL'
});

LSCP.Model.GameSession = Backbone.AssociatedModel.extend({

  persistableEntity: LSCP.Model.GameSessionPersist,

  defaults: {
    full_id: null,
    started_at: null,
    should_end_at: null,
    ended_at: null,
    time_limit: null,
    game: null,
    trials: [],
    events: [],
    config: null,
    subject: null,
    progress: 0,
    synced: false
  },

  relations: [
    {
      type: Backbone.One,
      key: 'game',
      relatedModel: 'LSCP.Model.Game'
    },
    {
      type: Backbone.Many,
      key: 'levels',
      collectionType: 'LSCP.Collection.LevelCollection'
    }
  ],

  validate: function(attributes){
    if(attributes.progress < 0 || attributes.progress > 100){
      return "Invalid progress (should be between 0 and 100)";
    }
  },

  initialize: function(){
    this.on("invalid", function(model, error){
      console.log('GameSession validation error:', error);
    });
    this.on('change:progress', function(){
      console.log('New progress:', this.get('progress'));
    });

    // Init trials counter
    this.trial_number = 0;
  },

  saveEvent: function(event, value){
    var e = {
      event: event,
      at: new Date()
    };
    if (typeof value !== 'undefined') e.value = value;
    this.set('events', this.get('events').concat(e));
    this.sync('update', this, ['events']).then(_.bind(function(){
      this.trigger('change');
    }, this));
  },

  hasCurrentTrial: function(){
    return (typeof this.current_trial !== 'undefined');
  },

  initTrial: function(){
    this.trial_number += 1;
    this.current_trial = {
      number: this.trial_number,
      started_at: new Date(),
      repetition: 0
    };
    console.log('initTrial', this.current_trial);
  },

  addTrialData: function(data){
    this.current_trial = _.extend(this.current_trial, data);
  },

  addTrialValue: function(key, value){
    this.current_trial[key] = value;
  },

  saveTrial: function(){
    if (!this.hasCurrentTrial()) return;
    this.set('trials', this.get('trials').concat(this.current_trial));
    this.sync('update', this, ['trials']).then(_.bind(function(){
      this.trigger('change');
    }, this));
  },

  isTimeLimitOver: function(){
    if ( this.get('should_end_at') == false ) {
      console.log('this test will not end by time limit');
      return false;
    }
    var now = new Date();
    var is_over = now > this.get('should_end_at');

    console.log('should_end_at', this.get('should_end_at'), 'now', now, 'is_over', is_over);

    return is_over;
  },

  setAsSynced: function(){
    this.set('synced', true);
    this.sync('update', this, ['synced']).then(_.bind(function(){
      this.trigger('change');
    }, this));
  },

  persistable_attributes: function(){
    var attr = _.pick(this.attributes, ['full_id', 'started_at', 'ended_at', 'time_limit', 'trials', 'events', 'config', 'subject', 'progress', 'synced']);
    attr.levels = this.get('levels').dump();
    return attr;
  },

  persistable: function(){
    var data = this.persistable_attributes();
    return new this.persistableEntity(data);
  },

  syncable_attributes: function(){
    return _.pick(this.attributes, ['full_id', 'started_at', 'ended_at', 'time_limit', 'levels', 'trials', 'events', 'config', 'subject']);
  }

});

LSCP.Model.Level = Backbone.AssociatedModel.extend({

    defaults: {
        name: "Untitled level",
        background: null,
        reward: null,
        introduce_objects: true,
        feedback: true,
        on_failure: null,
        stages: []
    },

    relations: [
        {
            type: Backbone.Many,
            key: 'stages',
            collectionType: 'LSCP.Collection.StageCollection'
        }
    ],
	
	initialize: function(){
//        console.log('LSCP.Model.Level.initialize');
    },

  persistable_attributes: function(){
    var attr = _.pick(this.attributes, ['name', 'background', 'introduce_objects', 'feedback', 'on_failure']);
    attr.stages = this.get('stages').dump();
    return attr;
  }



});
LSCP.Model.Session = Backbone.Model.extend({
	
	initialize: function(){
    this.set({
      started_at: new Date()
    });

    // TODO: logic to determine which games to play
    var game = new LSCP.Model.Game();
    this.games = new LSCP.Collection.GameCollection([game]);
  }

});
LSCP.Model.Stage = Backbone.Model.extend({

    defaults: {
      objects: [],
      objects_positions: null,
      time_idle: null,
      ask_for: null
    },
	
	initialize: function(){
    },

  persistable_attributes: function(){
    var attr = _.pick(this.attributes, ['objects', 'objects_positions', 'time_idle', 'ask_for']);
    return attr;
  }

});
LSCP.Model.Subject = Backbone.AssociatedModel.extend({

  defaults: {
    anonymous_id: '0'
  },

  initialize: function() {

    if (!('lscp.idevxxi.current_subject_id' in localStorage)) {
      this.setAnonymousId(this.defaults.anonymous_id);
    } else {
      this.set('anonymous_id', localStorage['lscp.idevxxi.current_subject_id']);
    }

  },

  setAnonymousId: function(id) {
    this.set('anonymous_id', id);
    localStorage['lscp.idevxxi.current_subject_id'] = id;
    this.trigger("change");
  },

  getAnonymousId: function() {
    return this.get('anonymous_id');
  }

});

LSCP.Collection.ConfigCollection = Backbone.Collection.extend({

    model: LSCP.Model.Config,
    url: LSCP.Locations.Backend + '/sync/config_profiles',
    comparator: 'name',

    initialize : function() {
      this.add(LSCP.Config.config_profiles);
    },

    hasCurrent: function() {
      return (typeof this.getCurrent().id !== 'undefined');
    },

    setCurrent: function(c) {
      localStorage['lscp.idevxxi.current_config'] = c;
      this.trigger('change');
    },

    getCurrent: function() {
      var id = localStorage['lscp.idevxxi.current_config'];
      return this.get(id) || new this.model({name: 'sin definir'});
    },

    getCurrentConfigContent: function() {
      if (!('lscp.idevxxi.current_config' in localStorage)) {
        throw "Please select a config profile.";
      }
      return JSON.parse(this.getCurrent().get('content'));
    }

});

LSCP.Collection.GameCollection = Backbone.Collection.extend({

    model : LSCP.Model.Game,

    initialize : function() {
    }

});
LSCP.Collection.GameObjectCollection = Backbone.Collection.extend({

  model: LSCP.Model.GameObject,
  url: LSCP.Locations.Backend + '/sync/game_objects',

  initialize: function() {
    this.add(LSCP.Config.game_objects);
  },

  count: function(filter){
    if (typeof filter === 'undefined') {
      return this.size();
    } else {
      return _.size(this.where(filter));
    }
  }

});
LSCP.Collection.GameSessionCollection = Backbone.Collection.extend({

  model: LSCP.Model.GameSession,
  url: LSCP.Locations.Backend + '/sync/update',

  initialize : function() {
    this.populateFromDatabase();
  },

  create: function(data){
    var deferred = $.Deferred();
    var game_session = this.add(data);
    console.log('Creating GameSession...', data, game_session);

    this.sync('create', game_session).then(function(ids){
      console.log('GameSession created!');
      game_session.set('id', _.first(ids));
      deferred.resolve(game_session);
    });

    return deferred;
  },

  populateFromDatabase: function(){
    console.log('GameSessionCollection.populateFromDatabase');
    this.sync('find', new this.model()).then(_.bind(function(data){
      console.log('GameSessionCollection.populateFromDatabase DONE');
      this.add(data);
      this.trigger('change');
      this.trigger('populatedFromDatabase');
    }, this));
    return this;
  },

  count: function(filter){
    if (typeof filter === 'undefined') {
      return this.length;
    } else {
      return this.where(filter).length;
    }
  },

  dump: function(filter){
    var models;
    var $that = this;

    if (typeof filter === 'undefined') {
      models = this.models;
    } else {
      models = this.where(filter);
    }

    var syncedSessionCount = this.count({synced: true});
    var subject = new LSCP.Model.Subject();

    models = models.map(function(m){
      var sessionsForBackend = {uuid: m.get('id'), data: m.syncable_attributes()};

      sessionsForBackend.data.full_id = $that.pad(subject.getAnonymousId(),2) + "-" + $that.pad(syncedSessionCount,4);
      syncedSessionCount++;

      return sessionsForBackend;
    });
    return models;
  },

  pad : function(n, width) {
      n = n + '';
      return n.length >= width ? n :
          new Array(width - n.length + 1).join('0') + n;
  },

  sendToBackend: function(){
    var $that = this;

    console.log('sendToBackend');
    var device = window.device;
    var data = {
      device: {
        uuid: device.uuid,
        os_version: device.version,
        model: device.model
      },
      sessions: this.dump({synced: false}).slice(0,5) //los primeros 5
    };

    if ( data.sessions.length == 0 ) {
      alert("Todos los resultados han sido sincronizados.");
      return;
    }

    $.ajax({
      type: 'POST',
      url: this.url,
      dataType: 'json',
      contentType: "application/json",
      data: JSON.stringify(data),
      processData: false,
      success: _.bind(function(data){
        console.log('sendToBackend response', data);
        if ( data.synced.length > 0 ) {
          _.each(data.synced, function(uuid){
            var gs = this.get(uuid);
            gs.setAsSynced();
          }, this);

          this.sendToBackend();
        } else {
          alert("No se pudieron guardar los datos en el servidor. Por favor, reporte este problema.");
        }

      }, this),
      error: function(jqXHT, textStatus, errorThrown){
        console.log('sendToBackend error!', errorThrown);
        alert("No se pueden enviar los datos al servidor, asegúrese de tener conexión a internet.");
      }
    });
  }

//  count: function(){
//    this.sync('count', new this.model()).then(_.bind(function(e){
//      this.persisted_length = e;
//      this.trigger('change');
//    }, this));
//    return this.persisted_length;
//  }

});

LSCP.Collection.LevelCollection = Backbone.Collection.extend({

    model: LSCP.Model.Level,

    initialize: function() {
    },

    dump: function(filter){
      var models;
      if (typeof filter === 'undefined') {
        models = this.models;
      } else {
        models = this.where(filter);
      }
      models = models.map(function(m){
        return m.persistable_attributes();
      });
      return models;
    }

});
LSCP.Collection.StageCollection = Backbone.Collection.extend({

    model: LSCP.Model.Stage,

    initialize: function() {
    },

    dump: function(filter){
      var models;
      if (typeof filter === 'undefined') {
        models = this.models;
      } else {
        models = this.where(filter);
      }
      models = models.map(function(m){
        return m.persistable_attributes();
      });
      return models;
    }

});

LSCP.View.Base = Backbone.View.extend({

	el: "#app",

  initialize: function() {
    console.log('LSCP.View.Base initialized!');
		this.game_sessions = this.subject = null;
  },

  events: {
      "touchstart #btn-start": "start",
      "mousedown #btn-start": "start",
      "touchstart #btn-dashboard": "openDashboard",
      "mousedown #btn-dashboard": "openDashboard"
  },

	ready: function() { //esto se llama desde utils.sync.js
		console.log("Base is ready");
		this.game_sessions = new LSCP.Collection.GameSessionCollection();
		this.subject = new LSCP.Model.Subject();

		this.listenTo(this.subject, "change", this.render);
		this.listenTo(this.game_sessions, "change", this.render);

		this.render();
	},

  start: function(e){
    e.stopPropagation(); e.preventDefault();

		LSCP.Session = null;
    $('#home').hide();
    LSCP.Session = new LSCP.View.Session();
		// LSCP.Session.startSession();
  },

  openDashboard: function(e){
    e.stopPropagation(); e.preventDefault();
    var password = window.prompt('Ingrese la contraseña');
    if (password !== LSCP.Auth.dashboard_password) {
      window.alert('No es correcta');
      return;
    }
    $('#home').hide();
    this.dashboard = new LSCP.View.Dashboard();
  },

	render : function() {
		if (this.subject == null || this.game_sessions == null) {
			return;
		}

		console.log("Base is rendering");
		$("#nextTestID").text( "ID: " + this.pad(this.subject.getAnonymousId(),2) + "-" + this.pad(this.game_sessions.length, 4) );
	},

	pad : function(n, width) {
      n = n + '';
      return n.length >= width ? n :
          new Array(width - n.length + 1).join('0') + n;
  }

});

LSCP.View.Dashboard = Backbone.View.extend({

	id: "dashboard",
	template_path: "templates/dashboard.html",

    initialize: function() {
      var self = this;

      self.game_sessions = new LSCP.Collection.GameSessionCollection();
      self.subject = new LSCP.Model.Subject();
      self.game_objects = new LSCP.Collection.GameObjectCollection();
      self.config_profiles = new LSCP.Collection.ConfigCollection();

      $.get(self.template_path, function(template) {
        self.template = _.template(template);

        LSCP.Sync.checkVersion().done(function(new_config){
          self.new_config = new_config;
          self.render();
        });

        self.listenTo(self.game_sessions, "change", _.throttle(self.render, 250));
        self.listenTo(self.subject, "change", self.render);
        self.listenTo(self.config_profiles, "change", _.throttle(self.render, 250));

        self.render();
      });
    },

    events: {
      "touchstart .close": "close",
      "touchstart #changeConfig": "changeConfig",
      "touchstart #syncUpload": "syncUpload",
      "touchstart #syncDownload": "syncDownload",
      "touchstart #changeSubjectId": "changeSubjectId",

      "mousedown .close": "close",
      "mousedown #changeConfig": "changeConfig",
      "mousedown #syncUpload": "syncUpload",
      "mousedown #syncDownload": "syncDownload",
      "mousedown #changeSubjectId": "changeSubjectId"
    },

    render: function(){
      this.data = {
        new_config: this.new_config || false,
        current_config_profile: this.config_profiles.getCurrent(),
        config_profiles: this.config_profiles,
        current_subject_id: this.subject.get('anonymous_id'),
        game_sessions: this.game_sessions,
        game_objects: this.game_objects
      };

      // console.log('render', this.data);
      var html = this.template(this.data);
      this.$el.html(html);
      if ($('#'+this.id).length === 0) {$('#app').append(this.$el);}
    },

    close: function(e) {
      e.stopPropagation(); e.preventDefault();
      this.remove();
      this.unbind();
			LSCP.App.render();
      $('#home').show();
    },

    changeConfig: function(e){
      e.stopPropagation(); e.preventDefault();
      var new_config_profile = $('.config select[name=config-local]').val();

			if ( new_config_profile != null ) {
				console.log("changeConfig", new_config_profile);
				this.config_profiles.setCurrent(new_config_profile);
			} else {
				alert("Debe hacer una primera sincronización antes de poder elegir la prueba.");
			}
    },

    changeSubjectId: function(e){
      e.stopPropagation(); e.preventDefault();
      var new_subject_id = $('.subject input[name=subject-id]').val();
      if (new_subject_id === '') return;
      console.log("changeSubjectId", new_subject_id);
      this.subject.setAnonymousId(new_subject_id);
    },

    syncUpload: function(e){
      e.stopPropagation(); e.preventDefault();
      this.game_sessions.sendToBackend();
    },

    syncDownload: function(e){
      e.stopPropagation(); e.preventDefault();
      var self = this;
      $('.modal-layer').show();

      LSCP.Sync.on('syncing', function(progress){
        $('.modal progress').attr(progress);
      });

			LSCP.Sync.on('syncingDone', function(){
        $('.modal-layer').hide();
        self.new_config = false;

        self.game_objects = new LSCP.Collection.GameObjectCollection();
        self.config_profiles = new LSCP.Collection.ConfigCollection();
        self.listenTo(self.config_profiles, "change", _.throttle(self.render, 250));

        self.render();

        // Reload, so the new config is taken into account
//        window.location.reload(false);
			});

      LSCP.Sync.downloadConfig();
    }

});

LSCP.View.Game = Backbone.View.extend({

    id : "game",
    game_session: null,
    speed: 2, // CHANGED SPEED TO SPEED UP
    subtitles: false,
    progressbar: null,
    reward: null,
    layersSize: {},
    idleInterval: null,
    idleTimer: 0,
    idleTime: 30, // seconds
    timeLimitInterval: null,
    imagesLoaded: $.Deferred(),
    soundsLoaded: $.Deferred(),

	  initialize: function(){
        console.log('LSCP.View.Game initialized!');

        this.game_session = this.model.get("session");

        this.progressbar = new LSCP.View.ProgressBar({model: this.game_session});
        this.reward = new LSCP.View.Reward();
        this.sound = LSCP.SoundManager.initialize();

        this.layersSize = {
            // width: 1024,
            // height: 768
            width: 1280,
            height: 800
        };

        this.pos = {
          CENTER_CENTER: {x: 'center', y: 'center'},
          CENTER_LEFT:   {x: 0,        y: 'center'},
          CENTER_RIGHT:  {x: 'right',  y: 'center'},
          TOP_LEFT:      {x: 0,        y: 40},
          TOP_RIGHT:     {x: 'right',  y: 40},
          BOTTOM_RIGHT:  {x: 'right',  y: this.layersSize.height - (300 + 40 + 50)},
          BOTTOM_LEFT:   {x: 0,        y: this.layersSize.height - (300 + 40 + 50)},
          FOR_1: ['CENTER_CENTER'],
          FOR_2: ['CENTER_LEFT', 'CENTER_RIGHT'],
          FOR_4: ['TOP_LEFT', 'TOP_RIGHT', 'BOTTOM_RIGHT', 'BOTTOM_LEFT']
        };

        $.when(this.imagesLoaded, this.soundsLoaded).then(this.start.bind(this));
	  },

    render: function(){
        console.log('LSCP.View.Game.render');

        // this.$el.html('').prepend(this.progressbar.el).append(this.reward.el);
        this.$el.html('').append(this.reward.el);

        return this;
    },

    events: {
        'mousedown': 'onTouch',
        'touchstart': 'onTouch'
    },


    // Game cycle

    start: function(){
        console.log('LSCP.View.Game starts!');
        this.startCheckingTimeLimit();
    },

    end: function(){
        console.log('LSCP.View.Game ends!');
        this.game_session.set({ended_at: new Date()});
        this.game_session.sync('update', this.game_session, ['ended_at']);
        this.stopWatchingIdle();
        this.stopCheckingTimeLimit();
        $('body').css('backgroundColor', 'black');
        this.progressbar.$el.remove();
        setTimeout(function(){
            this.trigger('end');
        }.bind(this), 5000 / this.speed);
    },


    // Game iteration management

    onIteration: function(){},
    onCorrectAnswer: function(){},
    onWrongAnswer: function(){},
    onNoAnswer: function(){},
    onIdle: function(){},


    // Game interaction

    onTouch: function(e){
      e.stopPropagation(); e.preventDefault();
      this.idleTimerReset();
    },


    // Idle time handling

    startWatchingIdle: function(){
        // Increment the idleTime every second
        if (this.idleInterval === null) {
            this.idleTimerReset();
            this.idleInterval = setInterval(this.idleTimerIncrement.bind(this), 1000);
        }
    },
    stopWatchingIdle: function(){
        clearInterval(this.idleInterval);
        this.idleInterval = null;
    },
    idleTimerReset: function(){
        this.idleTimer = 0;
    },
    idleTimerIncrement: function(){
        this.idleTimer = this.idleTimer + 1;

        if (this.idleTimer >= this.idleTime) {
            this.stopWatchingIdle();
            this.onIdle();
        }
    },


    // Time limit handling

    startCheckingTimeLimit: function(){
      if (this.timeLimitInterval === null) {
        this.timeLimitInterval = setInterval(this.checkTimeLimit.bind(this), 10000);
      }
    },
    stopCheckingTimeLimit: function(){
      clearInterval(this.timeLimitInterval);
      this.timeLimitInterval = null;
    },
    checkTimeLimit: function(){
      if (this.game_session.isTimeLimitOver()) {
        this.endGracefully();
      }
    },
    endGracefully: function(){
      console.log('endGracefully');
      this.stopWatchingIdle();

      this.game_session.saveEvent('end_gracefully');

      this.reward.show().on('end', function(){
        this.reward.hide().off('end');
        this.end();
      }.bind(this));
    },


    // Helpers to count time diff

    startMeasuringDiff: function(){
      this.diffStart = +new Date(); // get unix-timestamp in milliseconds
    },
    stopMeasuringDiff: function(){
      var diffStop = +new Date();
      var diff = diffStop - this.diffStart;
      this.diffStart = null;
      return diff;
    },


    // Game assets

    preloadImages: function(images){
        console.log('LSCP.View.Game is preloading images...');
        collie.ImageManager.add(images, function(){
            this.imagesLoaded.resolve();
        }.bind(this));
    },

    preloadSounds: function(sounds){
        // console.log('LSCP.View.Game is preloading sounds...');
        // console.log(sounds)
        this.sound.addSounds(sounds);
        this.soundsLoaded.resolve();
    }


});

LSCP.View.Home = Backbone.View.extend({

	id : "home",
	path : "home.html",
	
	initialize : function() {
	},

    events: {
        'click #dashboard-btn': 'dashboardClicked'
    },

    dashboardClicked: function () {
        console.log('Open Dashboard');
        return false;
    }
	
});

LSCP.Mandy = new Object({

    animations: {
        normal: {
            sprite: 'normal.png',
            values: [0],
            loop: 0
        },
        blink: {
            sprite: 'blink.png',
            values: [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
            loop: 0
        },
        ask: {
            sprite: 'ask.png',
            values: [0,1,2, 3,4,5,6,7,8,9,10,  11,12,13,14], //alargo el habla
            //values: [0,1,2, 3,4,5,6,7,8,9,10, 3,4,5,6,7,8,9,10, 3,4,5,6,7,8,9,10, 3,4,5,6,7,8,9,10, 3,4,5,6,7,8,9,10, 11,12,13,14], //alargo el habla
            loop: 1 //IMPORTANT!!! CHANGED FROM 1 TO 0 TO SPEED UP -- SEE ALSO ORIGINAL LINE PRECEDING
        },
        happy: {
            sprite: 'happy.png',
            values: [0,1,2,3,4,5,6,7,8,9,10,11,12,13,0],//[0,1,2,3,4,4,4,4,4,5,6,7,8,9],
//ORIGINAL            values: [0,1,2,3,4,4,4,4,4,5,6,7,8,9,1,10,11,12,12,12,12,12,13,6,7],
            loop: 1 //IMPORTANT!!! CHANGED FROM 2 TO 1 TO SPEED UP -- SEE ALSO ORIGINAL LINE PRECEDING
        },
        hello: {
            sprite: 'hello.png',
            values: [0],
//ORIGINAL            values: [0,1,2,3,4,5,6,7,8,9,10,11,12,13,10,11,14,15,16,17,18,19,16,17,20,21,22,23,0],
            loop: 1
        },
        sad: {
            sprite: 'sad.png',
            values: [0,1,2,3,4,5,6,6,6,6,6,6,7,8,8,8,8,8,8,8,8,8,8,9,9,9,10,11,12],
            loop: 1
        },
        bored: {
            sprite: 'bored.png',
            values: [0,1,2,3,4,5,6],// [0,1,2,3,4,5,6,7,8,7,6,7,9,10,11,12,13,12,11,12,13,12,11,14,15,16,17,18,19,20,21,20,22,23,24,23,25,26,27], // ORIGINAL [0,1,2,3,4,5,6,7,8,7,6,7,9,10,11,12,13,12,11,12,13,12,11,14,15,16,17,18,19,20,21,20,22,23,24,23,25,26,27]
            loop: 1
        },
        idle: {
            sprite: 'idle.png',
            values: [0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,2,2,2,2,2,2,2,2,2,2,2,2,1,1,1],
            loop: 1
        }
    },
    visible: false,
    currentAnimation: null,

    initialize: function() {
        console.log('LSCP.Mandy initialized!');

        _.each(this.animations, function(animation, id){
            var name = (id == 'normal') ? 'character' : 'character-' + id;
            collie.ImageManager.addImage(name, LSCP.Locations.Images + 'character/' + animation.sprite);
        });

    },

    addAnimations: function(parent) {
        var characters = {};

        _.each(this.animations, function(animation, id){
            if (id == 'normal') {
                characters.normal = new collie.DisplayObject({
                    backgroundImage: "character"
                }).addTo(parent);
            } else {
                characters[id] = new collie.DisplayObject({
                    backgroundImage: "character-" + id,
                    height: 400,
                    width: 400,
                    spriteLength: 35,
                    visible: false
                }).addTo(parent);
            }
        });

        return characters;
    },

    getTimers: function(characters){
        var timers = {};

        _.each(this.animations, function(animation, id){
            if (id == 'normal') return;

            timers[id] = collie.Timer.cycle(characters[id], "15fps", {
                loop: animation.loop,
                useAutoStart : false,
                valueSet: animation.values,
                onStart: function(){
                    _.each(characters, function(v){v.set('visible', false);});
                    characters[id].set('visible', true);
                    this.currentAnimation = id;
                }.bind(this),
                onComplete : function () {
                    if (this.currentAnimation != id) {return;}
                    characters[id].set('visible', false);
                    characters.normal.set('visible', true);
                    this.currentAnimation = null;
                }.bind(this)
            });
        });

        return timers;
    }

});


LSCP.View.ProgressBar = Backbone.View.extend({

    id: 'progressbar',

    initialize: function() {
        this.render();
        this.model.bind('change:progress', _.bind(this.render, this));
    },

    template: Handlebars.compile('<div class="bar" title="{{progress}}%"></div>'),

	  render: function() {
      var $bar = this.$el.find('.bar');
      if ($bar.length === 0) {
        $bar = this.$el.html(this.template(this.model.attributes)).find('.bar');
      }
      $bar.css('width', this.model.get('progress') + '%');
      return this;
	  },

    show: function() {
        this.$el.show();
        return this;
    },

    hide: function() {
        this.$el.hide();
        return this;
    }

});


LSCP.View.Reward = Backbone.View.extend({

    id: 'reward',
    images: [],
    previous_image_id: null,

    initialize: function() {
        console.log('LSCP.View.Reward initialized!');

        for (var i = 0; i < 9; i++) {
            this.images.push(LSCP.Locations.Images + "rewards/" + i + ".jpg");
        }

        this.hide();
    },

    template: Handlebars.compile('<img src="{{image}}" />'),

	render: function() {
        console.log('LSCP.View.Reward.render');

        var available_images = this.images.slice();

        if (this.previous_image_id !== null) {
            available_images.splice(this.previous_image_id, 1);
        }

        var id = _.random(0, _.size(available_images) - 1);
        this.$el.css('background-image', 'url(' + available_images[id] + ')').hide();
        this.previous_image_id = id;
        return this;
	},

    show: function() {
      this.render();
      this.$el.show().on('touchstart click', this.onClick.bind(this));
      return this;
    },

    hide: function() {
      this.$el.hide().off('touchstart click');
      return this;
    },

    onClick: function(e){
      e.stopPropagation(); e.preventDefault();
      this.trigger('end');
    }

});

LSCP.View.Session = Backbone.View.extend({

  el: "#session",

  config: null,
  game_sessions: null,
  subject: null,
  current_game: null,
  current_game_view: null,

  initialize: function(){
    console.log('LSCP.View.Session initialized!');

    this.subject = new LSCP.Model.Subject();
    this.game_sessions = new LSCP.Collection.GameSessionCollection();

    this.configs = new LSCP.Collection.ConfigCollection();

    console.log(this.configs.hasCurrent());
    if (!this.configs.hasCurrent()) {
      window.alert("Hay que seleccionar la prueba antes de comenzar.");
      this.endSession();
      return;
    }
    this.config = new LSCP.Model.Session(this.configs.getCurrentConfigContent().session);

    // console.log("synced:");
    // console.log(this.game_sessions.count({synced: true}));

    this.startSession();
  },

  render: function(){
      return this;
  },

  startSession: function(){

    this.current_game = this.config.games.shift();

    var now = new Date();
    var subject = new LSCP.Model.Subject();
    var should_end_at = new Date(now.getTime() + this.config.attributes.time_limit*1000);

    if ( this.config.attributes.time_limit == false ) {
      should_end_at = false;
    }

    var game_session_data = _.extend(this.config.attributes, {
      game: this.current_game,
      config: this.configs.getCurrent(),
      subject: subject.get('anonymous_id'),
      started_at: now,
      should_end_at: should_end_at
    });

    this.game_sessions.create(game_session_data).then(_.bind(function(game_session){

      this.current_game.set('session', game_session);

      this.current_game_view = new LSCP.View.WordComprehensionGame({
        model: this.current_game
      });

      this.$el.append(this.current_game_view.render().el);

      this.listenToOnce(this.current_game_view, 'end', this.endSession);

    }, this));

  },

  endSession: function(){
    this.config = null;
    this.current_game = null;
    if (this.current_game_view) {this.current_game_view.remove();}
    window.location.reload(false);
  }

});


LSCP.SoundManager = new Object({

    sounds: {},
    playing: {},
    debug: false,
    randomSpriteRegex: /(\D+)\d+$/,

    initialize: function() {
        this.log('LSCP.SoundManager initialized!');
        _.extend(this, Backbone.Events);
        return this;
    },

    log: function() {
        if (this.debug) console.log(arguments);
    },

    addSounds: function(sounds) {
        _.each(sounds, this.addSound, this);
        return this;
    },

    addSound: function(sound, name) {
        this.log('LSCP.SoundManager.addSound', sound, name);

        this.sounds[name] = new Howl( {src: sound.urls, sprite : sound.sprite } );

        // Clear listener after first call.
        // this.sounds[name].once('load', function() {
        //   console.log("loaded sound", this._src);
        // });

        // Manage random sprites
        this.sounds[name].randomSprites = {};
        if (typeof sound.sprite != 'undefined') {
            _.each(sound.sprite, function(sp, id){
                var match = id.match(this.randomSpriteRegex);
                if (match) {
                    var key = match[1]+'*';
                    if (_.has(this.sounds[name].randomSprites, key))
                        this.sounds[name].randomSprites[key]++;
                    else
                        this.sounds[name].randomSprites[key] = 1;
                }
            }, this);
        }

        return this;
    },

    play: function(sound, sprite) {
        this.log('LSCP.SoundManager.play', sound, sprite);

        // Manage random sprites
        if (typeof sprite != 'undefined' && sprite.indexOf('*') != -1) {
            sprite = sprite.replace('*', this.randomFromInterval(1, this.sounds[sound].randomSprites[sprite]));
        }

        // If the sound to play is not a plop, stop currently playing mandy sounds
        if (sound !== 'plop' && _.has(this.playing, 'mandy')) {
            this.playing.mandy.stop();
        }

        // Play the sound
        // console.log("play", sound, sprite);
        this.sounds[sound].play(sprite);
        return this.sounds[sound];

        // this.playing[sound] = this.sounds[sound].play(sprite).on('end', _.bind(function(){
        //   // After the sound played, remove it from this.playing
        //   delete this.playing[sound];
        // }, this));
        // return this;
    },

    delayedPlay: function(delay, sound, sprite) {
        _.delay(this.play.bind(this), delay, sound, sprite);
    },

    randomFromInterval: function(min,max) {
        return Math.floor(Math.random()*(max-min+1)+min);
    }

});

LSCP.View.WordComprehensionGame = LSCP.View.Game.extend({

  current_level: null,
  current_stage: null,
  current_trial: null,
  layers: {},
  objects: {},
  timers: {},
  $character: null,

  initialize: function(){
    LSCP.View.Game.prototype.initialize.apply(this, arguments);

    console.log('LSCP.View.WordComprehensionGame initialized!');

    // Preload assets
    var images = [
      ['star',         LSCP.Locations.Images + "star.png"],
      ['slot',         LSCP.Locations.Images + "slot-bg.png"],
      ['slot-correct', LSCP.Locations.Images + "slot-correct-bg.png"],
      ['slot-wrong',   LSCP.Locations.Images + "slot-wrong-bg.png"]
    ];
    var sounds = [
      ['mandy', {
        urls: [LSCP.Locations.Sounds + 'mandy/newsprite.mp3'],
        sprite: {
      		idle1: [189,555],
      		idle2: [1218,577],
      		right1: [2273,886],
      		right2: [3637,899],
      		right3: [4997,992],
      		right4: [6460,1357],
      		right5: [8282,900],
      		right6: [9637,1000]
       }
      }],
      ['plop', {urls: [LSCP.Locations.Sounds + 'plop.mp3']}]
    ];

    // Objects
    _.each(this.game_session.get('assets').objects, function(object){
      images.push(['object_' + object, LSCP.Locations.GameObjectImages + object + '.jpg']);
      // en vez de cargar todos los sonidos en cualquier orden
      // cargo solo los que van a sonar en el orden que van a sonar asi casi me aseguro de que ande bien siempre
      //
      // sounds.push(['object_' + object, {
      //   urls: [LSCP.Locations.GameObjectSoundSprites + object + '.mp3'],
      //   sprite: {
      //     ask:   [0, 2000],
      //     intro: [3000, 2000]
      //   }
      // }]);
    });

    // sonidos
    // cargo solo los que van a sonar en el orden que van a sonar asi casi me aseguro de que ande bien siempre
    // esto no necesariamente va a cargar en este orden. pero al menos va a cargar solo los que necesita y no todos
    this.game_session.get('levels').each( function(level) {
      level.get("stages").each( function(stage) {
        sounds.push(["object_" + stage.get("ask_for"), {
            urls: [LSCP.Locations.GameObjectSoundSprites + stage.get("ask_for") + '.mp3'],
            sprite: {
              ask: [0, 3000]
            }
        }]);
      });
    });

    // Backgrounds
    _.each(this.game_session.get('assets').backgrounds, function(background){
      images.push(["background_" + background, LSCP.Locations.Images + "backgrounds/" + background + ".jpg"]);
    });

    this.preloadImages(_.object(images));
    images = null;

    this.preloadSounds(_.object(sounds));
    sounds = null;
  },

  getCurrentLevel: function(){
    return this.game_session.get('levels').at(this.current_level);
  },

  getCurrentStage: function(){
    return this.getCurrentLevel().get('stages').at(this.current_stage);
  },

  render: function(){
    LSCP.View.Game.prototype.render.apply(this, arguments);
    console.log('LSCP.View.WordComprehensionGame.render');


    // Background

    this.layers.background = new collie.Layer(this.layersSize);


    // Object slots

    this.layers.slots = new collie.Layer({
      x: 40,
      y: 40,
      width: this.layersSize.width - 80,
      height: this.layersSize.height - 80
    });


    // Character

    this.layers.character = new collie.Layer(this.layersSize);
    this.objects.overlay = new collie.DisplayObject({
      backgroundColor: '#000',
      // height: 768,
      // width: 1024,
      width: 1280,
      height: 800,
      opacity: 1
    }).addTo(this.layers.character);
    this.objects.character = new collie.DisplayObject({
      x: "center",
      y: 800,
      height: 400,
      width: 400
    }).addTo(this.layers.character);

    LSCP.Mandy.initialize();
    this.objects.characters = LSCP.Mandy.addAnimations(this.objects.character);
    this.timers.characters = LSCP.Mandy.getTimers(this.objects.characters);


    // HUD

    this.layers.hud = new collie.Layer(this.layersSize);
    this.objects.hud_text = new collie.Text({
      x: "center",
      y: "bottom",
      fontColor: "#000",
      fontSize: 12,
      textAlign: 'center',
      width: this.layersSize.width,
      height: 100,
      visible: false
    }).addTo(this.layers.hud);
    if (this.subtitles) this.objects.subtitles = new collie.Text({
      x: "center",
      y: this.layersSize.height - 50,
      fontColor: "#FFF",
      fontSize: 20,
      fontWeight: "bold",
      textAlign: 'center',
      width: this.layersSize.width,
      height: 50
    }).addTo(this.layers.hud);


    // Rendering

    _.each(this.layers, function(l){
      collie.Renderer.addLayer(l);
    });
    collie.Renderer.load(this.el);
    collie.Renderer.start();

    return this;
  },


  // Game cycle

  start: function(){
    LSCP.View.Game.prototype.start.apply(this, arguments);
    console.log('LSCP.View.WordComprehensionGame starts!');

    this.current_level = 0;
    this.current_stage = 0;

    this.game_session.saveEvent('start');

    this.onIteration();
  },

  nextStage: function(){

    var level = this.getCurrentLevel();

    this.current_stage += 1;

    if (this.current_stage > level.get('stages').size() - 1) {
      this.game_session.saveEvent('show_reward');
      this.reward.show().on('end', function(){
        this.reward.hide().off('end');
        this.current_level += 1;
        this.current_stage = 0;

        this.game_session.saveEvent('touch_reward');

        if (this.current_level > this.game_session.get('levels').size() - 1) {
          this.end();
        } else {
          this.onIteration();
        }
      }.bind(this));
      return;
    }

    this.onIteration();
  },

  retryStage: function(){

    console.log("RETRY STAGE: level ", this.current_level, "stage", this.current_stage);

    this.game_session.addTrialValue('repetition', 1);

    this.onIteration();
  },

  end: function(){
    console.log('LSCP.View.WordComprehensionGame ends!');

    // Trial management
    this.game_session.saveTrial();

    LSCP.View.Game.prototype.end.apply(this, arguments);
    collie.Renderer.removeAllLayer();
    collie.Renderer.unload();
  },


  // Game iteration management

  onIteration: function(){
    LSCP.View.Game.prototype.onIteration.apply(this, arguments);

    console.log('onIteration', this.current_level, this.current_stage);

    var level = this.getCurrentLevel();
    var stage = this.getCurrentStage();

    // Trial management
    this.game_session.saveTrial();
    this.game_session.initTrial();
    this.game_session.addTrialData({
      level_number: this.current_level + 1,
      level_name: level.get('name'),
      level_background: level.get('background'),
      stage_number: this.current_stage + 1,
      objects_count: stage.get("objects").length
    });

    // Progress
    //if (this.current_stage === 0) this.game_session.set({progress: 0});//IMPORTANT!! PROGRESS BAR REMOVED TO SPEED UP

    // Set idle time for the current stage
    this.idleTime = stage.get('time_idle');

    // Background
    if (this.objects.background) this.layers.background.removeChild(this.objects.background);
    this.objects.background = new collie.DisplayObject({
      x: "center",
      y: "center",
      backgroundImage: "background_" + level.get('background'),
      height: 800,
      width: 1280,
      opacity: 1
    }).addTo(this.layers.background);


    // Object slots
    this.objects.slots = [];

    // "Tutorial" mode when only one object
//        var introduce_objects = true;
//        var show_character = true;
//        if (stage.get("objects").length == 1) {
//            introduce_objects = true;
//            show_character = false;
//        }

    // Override objects positions
    this.objects_positions = [];
    if (stage.get("objects_positions") === 'NATURAL') {
      this.objects_positions = this.pos['FOR_' + stage.get("objects").length];
    } else if (stage.get("objects_positions") === 'RANDOM') {
      this.objects_positions = window.shuffleArray(this.pos['FOR_' + stage.get("objects").length]);
    } else if (_.isArray(stage.get("objects_positions"))) {
      this.objects_positions = stage.get("objects_positions");
    } else {
      throw 'Wrong value for "objects_positions" on level '+this.current_level+' stage '+this.current_stage;
    }

    // Create slots
    if (stage.get("objects_positions") === 'RANDOM') {
      stage.set("objects", window.shuffleArray(stage.get("objects")));
    }
    _.each(stage.get("objects"), function(object, i){
      var position = this.objects_positions[i];
      if (typeof this.pos[position] === 'undefined') throw 'Wrong value "'+position+'" for "objects_positions" on level '+this.current_level+' stage '+this.current_stage;
      var slot = new collie.DisplayObject({
        backgroundImage: "slot",
        opacity: 0
      }).set(this.pos[position]).addTo(this.layers.slots);
      new collie.DisplayObject({
        backgroundImage: 'object_' + object
      }).addTo(slot).align('center', 'center', slot);
      this.objects.slots.push(slot);
      this.game_session.addTrialValue("object"+(i+1)+"_name", object);

      if (stage.get("ask_for") === object) {
        this.game_session.addTrialData({
          object_asked: stage.get('ask_for'),
          object_asked_position: position
        });
      }
    }, this);


    // HUD
    this.objects.hud_text.text('LEVEL: ' + level.get('name'));


    // Display queue

    collie.Timer.queue().

//IMPORTANT!!! MODIFY TIMING OF IMAGE APPEARANCE HERE
      delay(function(){
        this.objects.hud_text.set({visible: true});
      }.bind(this), 10 / this.speed). //IMPORTANT TIME CHANGED FROM 1000 TO 10 TO SPEED UP

      transition(this.objects.overlay, 10 / this.speed, {//IMPORTANT TIME CHANGED FROM 1000 TO 10 TO SPEED UP
        from: 1,
        to: 0,
        set: "opacity",
        effect: collie.Effect.easeOutQuint
      }).

      delay(function(){
        if (level.get('introduce_objects') === true) {
          this.introduceObject(this.objects.slots[0], 0);
        }
        else {
          _.each(this.objects.slots, function(slot, i){
            setTimeout(function(){
              collie.Timer.transition(this.objects.slots[i], 1000 / this.speed, {
                from: 0,
                to: 1,
                set: "opacity",
                effect: collie.Effect.easeOutQuint
              });
              if (i === stage.get("objects").length - 1) {
                setTimeout(this.onObjectsIntroduced.bind(this), 4000 / this.speed); //IMPORTANT -- TIME CHANGED FROM 2K TO 4K TO ALLOW MORE INSPECTION TIME
              }
            }.bind(this), 0 * i / this.speed);//IMPORTANT TIME CHANGED FROM !!1500!! TO !!ZERO!! TO SPEED UP
          }.bind(this));
        }
      }.bind(this), 0)

    ;

  },

  introduceObject: function(slot, i){
    var stage = this.getCurrentStage();
    this.startMeasuringDiff();

    this.sound.play('object_' + stage.get('objects')[i], 'intro');

    collie.Timer.queue().

      transition(slot, 1000 / this.speed, {
        from: 0,
        to: 1,
        set: "opacity",
        effect: collie.Effect.easeOutQuint
      }).

      delay(function(){
        if (this.subtitles) this.objects.subtitles.set({visible: true}).text("♫ This is " + stage.get('objects')[i]);

        this.startWatchingIdle();

        slot.attach({
          mousedown: function () {
            var diff_time = this.stopMeasuringDiff();

            this.sound.play('plop');
            var currentY = slot.get('y');
            collie.Timer.transition(slot, 400 / this.speed, {
              to: currentY - 40,
              set: "y",
              effect: collie.Effect.wave(2, 0.25)
            });

            this.stopWatchingIdle();

            if (this.subtitles) this.objects.subtitles.set({visible: false});

            _.invoke(this.objects.slots, 'set', {backgroundColor: 'rgba(255,255,255,0)'});
            _.invoke(this.objects.slots, 'detachAll');

            this.game_session.addTrialValue("object"+(i+1)+"_touch_diff_time", diff_time);
            this.game_session.saveEvent('touch_object', stage.get('objects')[i]);

            setTimeout(function(){
              collie.Timer.transition(this.objects.slots[i], 200 / this.speed, {
                from: 1,
                to: 0.3,
                set: "opacity",
                effect: collie.Effect.easeOutQuint
              });

              if (i < stage.get("objects").length - 1) {
                i++;
                this.introduceObject(this.objects.slots[i], i);
              } else {
                this.onObjectsIntroduced();
              }
            }.bind(this), 2000 / this.speed);
          }.bind(this)
        });
      }.bind(this), 0)

    ;

  },

  onCorrectAnswer: function(slot){
    LSCP.View.Game.prototype.onCorrectAnswer.apply(this, arguments);

    var level = this.getCurrentLevel();
    this.game_session.addTrialValue('correct', 1);

    // Idle
    this.stopWatchingIdle();

    if (level.get('feedback') === true) {
      // Animation
      this.timers.characters.happy.start();

      // Sound
      this.sound.play('mandy', 'right*');
      if (this.subtitles) this.objects.subtitles.set({visible: true}).text("♫ BRAVO!");

      this.game_session.addTrialValue('feedback', 1);
    } else {
      this.game_session.addTrialValue('feedback', 0);
    }

    // Progress //IMPORTANT!! PROGRESS BAR REMOVED TO SPEED UP
    //var progress = 100 / level.get('stages').length * (this.current_stage+1);
    //this.game_session.set({progress: Math.floor(progress)});

    // Display queue
    collie.Timer.queue().

      delay(function(){
        slot.set('backgroundImage', 'slot-correct');
      }.bind(this), 0).

      delay(function(){
        if (this.subtitles) this.objects.subtitles.set({visible: false});
      }.bind(this), 0).

      delay(function(){

        collie.Timer.transition(this.objects.overlay, 800 / this.speed, {
          from: 0,
          to: 1,
          set: "opacity",
          effect: collie.Effect.easeOutQuint
        });
        collie.Timer.transition(this.objects.character, 1000 / this.speed, {
          from: 1,
          to: 0,
          set: "opacity",
          effect: collie.Effect.easeOutQuint
        });

      }.bind(this), 3000 / this.speed).

      delay(function(){
        LSCP.Mandy.visible = false;
        this.objects.character.set({
          opacity: 1,
          y: 800
        });
        this.layers.slots.removeChildren(this.objects.slots);
        this.nextStage();
      }.bind(this), 2000 / this.speed)

    ;
  },

  onWrongAnswer: function(slot){
    LSCP.View.Game.prototype.onWrongAnswer.apply(this, arguments);

    var level = this.getCurrentLevel();
    this.game_session.addTrialValue('correct', 0);

    // Idle
    this.stopWatchingIdle();

    if (level.get('feedback') === true) {
      // Animation
//      this.timers.characters.sad.start();//IMPORTANT!!! COMMENTED OUT SO AS TO MAKE THE GAME FASTER!!! NO NEGATIVE FEEDBACK!!!


      // Sound
//      this.sound.play('mandy', 'wrong*');//IMPORTANT!!! COMMENTED OUT SO AS TO MAKE THE GAME FASTER!!! NO NEGATIVE FEEDBACK!!!

      if (this.subtitles) this.objects.subtitles.set({visible: true}).text("♫ NO, YOU'RE WRONG");

      // Slot
//      slot.set('backgroundImage', 'slot-wrong'); //IMPORTANT!!! COMMENTED OUT SO AS TO MAKE THE GAME FASTER!!! NO NEGATIVE FEEDBACK!!!

      this.game_session.addTrialValue('feedback', 1);
    } else {
      slot.set('backgroundImage', 'slot-correct');

      this.game_session.addTrialValue('feedback', 0);
    }

    // Display queue
    collie.Timer.queue().

      delay(function(){
        if (this.subtitles) this.objects.subtitles.set({visible: false});
      }.bind(this), 0).

      delay(function(){

        collie.Timer.transition(this.objects.overlay, 800 / this.speed, {
          from: 0,
          to: 1,
          set: "opacity",
          effect: collie.Effect.easeOutQuint
        });
        collie.Timer.transition(this.objects.character, 1000 / this.speed, {
          from: 1,
          to: 0,
          set: "opacity",
          effect: collie.Effect.easeOutQuint
        });

      }.bind(this), 3000 / this.speed).

      delay(function(){
        LSCP.Mandy.visible = false;
        this.objects.character.set({
          opacity: 1,
          y: 800
        });
        this.layers.slots.removeChildren(this.objects.slots);

        switch (level.get('on_failure')) {
          // Handling of different possible behaviors in case of failure

          case 'REPEAT_STAGE':
            this.retryStage();
            break;

          case 'CONTINUE':
            // Progress//IMPORTANT!! PROGRESS BAR REMOVED TO SPEED UP
            //var progress = 100 / level.get('stages').length * (this.current_stage+1);
            //this.game_session.set({progress: Math.floor(progress)});

            this.nextStage();
            break;
        }

      }.bind(this), 2000 / this.speed)

    ;
  },

  onNoAnswer: function(){
    LSCP.View.Game.prototype.onNoAnswer.apply(this, arguments);
  },

  onIdle: function(){
    LSCP.View.Game.prototype.onIdle.apply(this, arguments);

    if (LSCP.Mandy.visible) {
      this.sound.play('mandy', 'idle*');
      this.timers.characters.bored.start();
    } else {
      this.sound.play('mandy', 'idle*');
    }
  },


  // Game interaction

  onTouch: function(){
    LSCP.View.Game.prototype.onTouch.apply(this, arguments);
  },

  onObjectsIntroduced: function(){

    collie.Timer.queue().

	  //cover things up
      transition(this.objects.overlay, 1000 / this.speed, {
        from: 0,
        to: 0.9,
        set: "opacity",
        effect: collie.Effect.easeOutQuint
      }).

      transition(this.objects.character, 1000 / this.speed, {
        to: 200,
        set: "y",
        effect: collie.Effect.easeOutQuint
      }).

      delay(function(){
        this.startMeasuringDiff();
        this.startWatchingIdle();
        LSCP.Mandy.visible = true;
        this.timers.characters.hello.start();
//        this.sound.delayedPlay(600, 'mandy', 'hello*');//IMPORTANT!!! COMMENTED OUT TO SPEED UP GAME BY HAVING NO HELLO!

        this.objects.character.set({backgroundColor: 'rgba(255,255,255,0.1)'})
// ORIGINAL CODE
         .attach({
           mousedown: function () {//IMPORTANT
             this.objects.character.set({backgroundColor: 'rgba(255,255,255,0)'});
             this.objects.character.detachAll();

             this.game_session.addTrialValue("mandy_touch_diff_time", this.stopMeasuringDiff());
             this.stopWatchingIdle();
             this.onTouchCharacter();
           }.bind(this)
         });
      }.bind(this), 0)

    ;
  },

  onTouchCharacter: function(){
    this.sound.play('plop');
    var stage = this.getCurrentStage();
    var started_asking_at;

    this.game_session.saveEvent('touch_mandy');

    collie.Timer.queue().

      delay(function(){
        var askAnimation = this.timers.characters.ask;
        askAnimation.start();

        var sound = this.sound.play('object_' + stage.get('ask_for'), 'ask');
        // sound.once("end", function() {
        //   console.log("ended sound");
        //   askAnimation.stop();
        // });
        // esto tarda en tirar end, no se si en la tablet pasa lo mismo pero me parece que si

        started_asking_at = +new Date() + 500;
        if (this.subtitles) this.objects.subtitles.set({visible: true}).text("♫ Where is the " + stage.get('ask_for') + "?");
      }.bind(this), 500 / this.speed).

      delay(function(){
        _.each(this.objects.slots, function(slot, i){
          slot.set({opacity: 1});
          slot.attach({
            mousedown: function () {
              this.sound.play('plop');
              this.game_session.saveEvent('touch_object', stage.get("objects")[i]);
              var object_touched_at = +new Date() - started_asking_at;
              this.game_session.addTrialData({
                object_touched: stage.get("objects")[i],
                object_touched_position: this.objects_positions[i],
                object_touched_at: object_touched_at,
                stage_end: 'touch_object'
              });

              _.invoke(this.objects.slots, 'detachAll');

              var currentY = slot.get('y');
              var slots_to_hide = _.reject(this.objects.slots, function(s){return s === slot;});

              collie.Timer.queue().

                transition(slot, 400 / this.speed, {
                  to: currentY - 40,
                  set: "y",
                  effect: collie.Effect.wave(2, 0.25)
                }).

                transition(slots_to_hide, 400 / this.speed, {
                  from: 1,
                  to: 0,
                  set: "opacity"
                }).

                delay(function(){
                  if (stage.get("objects")[i] == stage.get("ask_for"))
                    this.onCorrectAnswer(slot);
                  else
                    this.onWrongAnswer(slot);
                }.bind(this), 500 / this.speed);

            }.bind(this)
          });
        }, this);

        this.startWatchingIdle();
      }.bind(this), 2000 / this.speed).

      transition(this.objects.overlay, 1000 / this.speed, {
        from: 0.9,
        to: 0,
        set: "opacity",
        effect: collie.Effect.easeOutQuint
      })

    ;

  }


});
